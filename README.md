## checkers-rs

This is the backend for the checkers app.

It uses PostgreSQL to store information, and actor-based websockets server
to provide clients with data.
