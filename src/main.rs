extern crate actix_web;

use actix_web::{App, fs::StaticFiles, middleware, server};

use std::env;

fn main() {
    println!("Checkers.RS server started...");

    if env::args().len() > 1 {
        let path = env::args().nth(1).unwrap();

        println!("(Web interface: http://127.0.0.1:9088/)");

        server::new(move ||
            App::new()
                .middleware(middleware::Logger::default())
                .handler("/", StaticFiles::new(path.clone()).unwrap())
        )
            .bind("127.0.0.1:9088")
            .unwrap()
            .run();
    }

    println!("Checkers.RS server stopped");
}
